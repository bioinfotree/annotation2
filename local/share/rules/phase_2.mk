# Copyright Michele Vidotto 2016 <michele.vidotto@gmail.com>

PRJ ?=
INPUT_FASTA ?= 

SPECIES ?=

ADDRESSES ?=
README ?=
EVAL_BLAST ?=
EVAL_FILTER ?=
QUERY_COV ?=





# produce variables that link blast database belonging to phase_2 for each specie in $(SPECIES) with extern
# generate a makefile that is subsequently evaluated.
addresses.mk:
	>$@; \
	array1=($(SPECIES)); \
	count=$${#array1[@]}; \
	for i in `seq 1 $$count`; \
	do \
	echo -e "extern ../../../../454_preprocessing_2/dataset/acdna/phase_2/$${array1[$$i-1]}.flag as $${array1[$$i-1]}_LINK" >>$@; \
	done


include addresses.mk



# links
query.fasta:
	zcat <$(INPUT_FASTA) >$@


# use extern file asseciated to corresponding variable
%.flag:
	ln -sf $(basename $($*_LINK)) $(basename $@); \
	touch $@

%.xml.gz: query.fasta %.flag
	!threads
	$(call search_DB_tblastx, $<, ./$(basename $^2)/$(basename $^2), $@)

%.tab: %.xml.gz
	$(call parse_result, $<, $@)


define search_DB_tblastx
	$(call load_modules); \
	tblastx -evalue $(EVAL_BLAST) -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $1 -db $2 -out $(basename $3); \
	if gzip -cv $(basename $3) > $3; \
	then \
	rm $(basename $3); \
	fi
endef


define parse_result
	zcat -dc $1 | blast_fields -f \
	-r application,database_length,database,database_sequences,expect,query,query_length \
	-a accession,hit_def,hit_id,length \
	-p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand \
	-e $(EVAL_FILTER) \
	-q $(QUERY_COV) \
	>$2
endef



.PHONY: test
test:
	@echo $(ADDRESSES)



ALL += $(addsuffix .flag, $(SPECIES)) \
	 $(addsuffix .xml.gz, $(SPECIES)) \
	 $(addsuffix .tab, $(SPECIES))

INTERMEDIATE += 

CLEAN += 